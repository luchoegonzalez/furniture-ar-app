using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemButtonManager : MonoBehaviour
{
    public string nombreItem;
    public string descripcionItem;
    public Sprite imagenItem;
    public GameObject item3DModel;

    private ARInteractionManager interactionManager;

    void Start()
    {
        transform.GetChild(0).GetComponent<TMP_Text>().text = nombreItem;
        transform.GetChild(1).GetComponent<RawImage>().texture = imagenItem.texture;
        transform.GetChild(2).GetComponent<TMP_Text>().text = descripcionItem;

        var boton = GetComponent<Button>();
        boton.onClick.AddListener(GameManager.instance.ARPosition);
        boton.onClick.AddListener(CrearModelo3D);

        interactionManager = FindObjectOfType<ARInteractionManager>();
    }

    private void CrearModelo3D()
    {
        interactionManager.Modelo3Ditem = Instantiate(item3DModel);
    }
}
