using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARInteractionManager : MonoBehaviour
{
    [SerializeField] private Camera camaraAR;
    private ARRaycastManager raycastManagerAR;
    private List<ARRaycastHit> hits = new List<ARRaycastHit>();

    private GameObject pointerAR;
    private GameObject modelo3Ditem;
    private GameObject itemSeleccionado;

    private bool enPosicionInicial;
    private bool tocandoUI;
    private bool tocandoModelo3D;

    private Vector2 posicionInicialToque;

    public GameObject Modelo3Ditem
    {
        set
        {
            modelo3Ditem = value;
            modelo3Ditem.transform.position = pointerAR.transform.position;
            modelo3Ditem.transform.parent = pointerAR.transform;
            enPosicionInicial = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        pointerAR = transform.GetChild(0).gameObject;
        raycastManagerAR = FindObjectOfType<ARRaycastManager>();
        GameManager.instance.OnMainMenu += SetearPosicionItem;
    }

    // Update is called once per frame
    void Update()
    {
        if (enPosicionInicial)
        {
            Vector2 middlePointScreen = new Vector2(Screen.width / 2, Screen.height / 2);
            raycastManagerAR.Raycast(middlePointScreen, hits, TrackableType.Planes);
            if (hits.Count > 0)
            {
                transform.position = hits[0].pose.position;
                transform.rotation = hits[0].pose.rotation;
                pointerAR.SetActive(true);
                enPosicionInicial = false;
            }
        }

        if (Input.touchCount>0)
        {
            Touch toque1 = Input.GetTouch(0);

            if (toque1.phase == TouchPhase.Began)
            {
                Vector3 posicionDeToque = toque1.position;
                tocandoUI = isTapOverUI(posicionDeToque);
                tocandoModelo3D = isTap3DModel(posicionDeToque);
            }

            if (toque1.phase == TouchPhase.Moved)
            {
                if (raycastManagerAR.Raycast(toque1.position, hits, TrackableType.Planes))
                {
                    Pose hitPose = hits[0].pose;
                    if (!tocandoUI && tocandoModelo3D)
                    {
                        transform.position = hitPose.position;
                    }
                }
            }

            if (Input.touchCount == 2)
            {
                Touch toque2 = Input.GetTouch(1);
                if (toque1.phase == TouchPhase.Began || toque2.phase == TouchPhase.Began)
                {
                    posicionInicialToque = toque2.position - toque1.position;
                }

                if (toque1.phase == TouchPhase.Moved || toque2.phase == TouchPhase.Moved)
                {
                    Vector2 posicionActualToque = toque2.position - toque1.position;
                    float angulo = Vector2.SignedAngle(posicionInicialToque, posicionActualToque);
                    modelo3Ditem.transform.rotation = Quaternion.Euler(0, modelo3Ditem.transform.eulerAngles.y - angulo, 0);
                    posicionInicialToque = posicionActualToque;
                }
            }

            if (tocandoModelo3D && modelo3Ditem == null && !tocandoUI)
            {
                GameManager.instance.ARPosition();
                modelo3Ditem = itemSeleccionado;
                itemSeleccionado = null;
                pointerAR.SetActive(true);
                transform.position = modelo3Ditem.transform.position;
                modelo3Ditem.transform.parent = pointerAR.transform;
            }
        }
    }

    private void SetearPosicionItem()
    {
        if (modelo3Ditem != null)
        {
            modelo3Ditem.transform.parent = null;
            pointerAR.SetActive(false);
            modelo3Ditem = null;
        }
    }

    public void borrarItem()
    {
        Destroy(modelo3Ditem);
        pointerAR.SetActive(false);
        GameManager.instance.MainMenu();
    }

    private bool isTap3DModel(Vector3 posicionDeToque)
    {
        Ray rayo = camaraAR.ScreenPointToRay(posicionDeToque);
        if (Physics.Raycast(rayo, out RaycastHit hitModelo3D))
        {
            if (hitModelo3D.collider.CompareTag("item"))
            {
                itemSeleccionado = hitModelo3D.transform.gameObject;
                return true;
            }
        }
        return false;
    }

    private bool isTapOverUI(Vector3 posicionDeToque)
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = new Vector2(posicionDeToque.x, posicionDeToque.y);

        List<RaycastResult> resultado = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, resultado);

        return resultado.Count > 0;
    }
}
