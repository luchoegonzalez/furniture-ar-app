using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    [SerializeField] private List<Item> items = new List<Item>();
    [SerializeField] private GameObject contenedorDeBotones;
    [SerializeField] private ItemButtonManager itemButtonManager;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.OnItemsMenu += Crearbotones;
    }

    private void Crearbotones()
    {
        foreach (var item in items)
        {
            ItemButtonManager itemButton;
            itemButton = Instantiate(itemButtonManager, contenedorDeBotones.transform);
            itemButton.nombreItem = item.nombre;
            itemButton.descripcionItem = item.descripcion;
            itemButton.imagenItem = item.imagen;
            itemButton.item3DModel = item.modelo3D;
            itemButton.name = item.nombre;
        }

        GameManager.instance.OnItemsMenu -= Crearbotones;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
