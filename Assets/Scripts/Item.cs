using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Item : ScriptableObject
{
    public string nombre;
    public Sprite imagen;
    public string descripcion;
    public GameObject modelo3D;
}
