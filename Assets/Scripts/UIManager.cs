using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject mainMenuCanvas;
    [SerializeField] private GameObject itemsMenuCanvas;
    [SerializeField] private GameObject arPositionCanvas;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.OnMainMenu += ActivarMainMenu;
        GameManager.instance.OnItemsMenu += ActivarItemsMenu;
        GameManager.instance.OnARPosition += ActivarARPosition;
    }

    private void ActivarMainMenu()
    {
        mainMenuCanvas.transform.GetChild(0).transform.DOScale(Vector3.one, 0.3f);
        mainMenuCanvas.transform.GetChild(1).transform.DOScale(Vector3.one, 0.3f);
        mainMenuCanvas.transform.GetChild(2).transform.DOScale(Vector3.one, 0.3f);

        itemsMenuCanvas.transform.GetChild(0).DOScale(Vector3.zero, 0.5f);
        itemsMenuCanvas.transform.GetChild(1).DOScale(Vector3.zero, 0.3f);
        itemsMenuCanvas.transform.GetChild(1).DOMoveY(180, 0.3f);

        arPositionCanvas.transform.GetChild(0).DOScale(Vector3.zero, 0.3f);
        arPositionCanvas.transform.GetChild(1).DOScale(Vector3.zero, 0.3f);
    }


    private void ActivarItemsMenu()
    {
        mainMenuCanvas.transform.GetChild(0).transform.DOScale(Vector3.zero, 0.3f);
        mainMenuCanvas.transform.GetChild(1).transform.DOScale(Vector3.zero, 0.3f);
        mainMenuCanvas.transform.GetChild(2).transform.DOScale(Vector3.zero, 0.3f);

        itemsMenuCanvas.transform.GetChild(0).DOScale(Vector3.one, 0.3f);
        itemsMenuCanvas.transform.GetChild(1).DOScale(Vector3.one, 0.3f);
        itemsMenuCanvas.transform.GetChild(1).DOMoveY(350, 0.3f);
    }


    private void ActivarARPosition()
    {
        mainMenuCanvas.transform.GetChild(0).transform.DOScale(Vector3.zero, 0.3f);
        mainMenuCanvas.transform.GetChild(1).transform.DOScale(Vector3.zero, 0.3f);
        mainMenuCanvas.transform.GetChild(2).transform.DOScale(Vector3.zero, 0.3f);

        itemsMenuCanvas.transform.GetChild(0).DOScale(Vector3.zero, 0.5f);
        itemsMenuCanvas.transform.GetChild(1).DOScale(Vector3.zero, 0.3f);
        itemsMenuCanvas.transform.GetChild(1).DOMoveY(180, 0.3f);

        arPositionCanvas.transform.GetChild(0).DOScale(Vector3.one, 0.3f);
        arPositionCanvas.transform.GetChild(1).DOScale(Vector3.one, 0.3f);
    }

}
